#!/usr/bin/env python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
import re, platform
__metaclass__ = type

DOCUMENTATION = r'''
---
module: manage_hosts

short_description: This module manages entries in system hosts file

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This modules manages entries in system hosts file. It adds names to existings one if ip already exists.

options:
    entries:
        description: An array of managed entries. Each entry must contain one ip and names keys
        required: true
        type: list

author:
    - Guillaume Rémy (guillaume.remy@csi-remy.pro)
'''

EXAMPLES = r'''
- name: Manage hosts entries
  manage_hosts:
    entries:
        - ip: 10.0.2.1
          names:
            - http1
            - http1.mydomain.com
        - ip: 10.0.2.2
          names: ["bdd1", "bdd1.mydomain.com"]
'''

RETURN = r'''
requested_hosts_entries:
    description: The original entries param that was passed in.
    type: list
    returned: always
    sample: [{ip: 10.0.2.1,
          names: ["http1", "http1.mydomain.com"]},
        {ip: 10.0.2.2,
          names: ["bdd1", "bdd1.mydomain.com"]}]
hosts_file_content:
    description: The resulting hosts file.
    type: string
    returned: always
    sample: "10.0.2. bdd1 bdd1.mydomain.com"
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        entries=dict(type='list', required=True),
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        requested_hosts_entries=[],
        hosts_file_content=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    result['requested_hosts_entries'] = str(module.params['entries'])

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if len(module.params['entries']) == 0:
        module.fail_json(msg='No host entries found', **result)
    
    if platform.system() == 'Linux':
        filename = '/etc/hosts'
    else:
        filename = "C:\Windows\System32\Drivers\etc\hosts"
    
    with open(filename, 'r') as fd:
        content = fd.read()
    result['hosts_file_content'] = str(content)
    content_new = content
    for entry in module.params['entries']:
        if "ip" in entry and "names" in entry:
            for name in entry.get('names'):
                already_existing = re.findall(entry.get('ip')+r'.*'+r'\s'+name+r'\s', content_new)
                if len(already_existing) == 0:
                    existing_ip = re.findall(entry.get('ip'), content_new)
                    if len(existing_ip) == 0:
                        content_new = content_new + "\n" + entry.get('ip')+ ' ' + name
                    else:
                        content_new = re.sub(entry.get('ip')+r'\s*(.*)', entry.get('ip')+r' \1 '+name, content_new,
                                             flags=re.M)
    if content_new != content:
        result['changed'] = True
        result['hosts_file_content'] = str(content_new)
        if not module.check_mode:
            with open(filename, 'w') as fd:
                fd.write(content_new)
    
    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()

